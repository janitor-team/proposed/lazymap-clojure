# lazymap – take the strictness out of maps

## Description

LazyMap is a transparent wrapper around the map types of Clojure. It works
similar to lazy-seq for sequences: the value associated with a key via
lazy-assoc is not evaluated until it is actually accessed. So if the value is
dissoc'd before it is accessed the code necessary to generate the value is not
executed at all.

## Usage

    user=> (use 'lazymap.core)
    nil
    user=> (def m (lazy-hash-map :x (do (println :a) :y)))
    #'user/m
    user=> (get m :x)
    :a
    :y
    user=> (get m :x)
    :y
    user=>

### Gradle

    dependencies { compile 'de.kotka:lazymap:3.0.0' }

### Maven

    <dependency>
      <groupId>de.kotka</groupId>
      <artifactId>lazymap</artifactId>
      <version>3.0.0</version>
    </dependency>

### Leiningen

    :dependencies [[de.kotka/lazymap "3.0.0"]]

Meikel Brandmeyer <mb@kotka.de>,<br>
Frankfurt am Main, 2011
